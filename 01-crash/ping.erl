    -module(ping).

    -export([init/0, init/2]).

    init() ->
        {pong_ref, 'pong@ip_computadora'} ! {self(), "Envio un ping!"},
        receive
            Msg -> io:format("Ping recibe: ~s~n", [Msg])
        end.

    init(X, Y) ->
        {X, Y} ! {self(), "Envio un ping!"},
        receive
            Msg -> io:format("Ping recibe: ~s~n", [Msg])
        end.