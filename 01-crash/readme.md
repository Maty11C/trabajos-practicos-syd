# Ejercicio: Ping - Pong

## Solución

### 1º Pong

- En una terminal (cmd, linea de comandos) levantar un nodo:
```
$ erl -name pong@ip_computadora -setcookie secret 
```

- Compilar y cargar el archivo pong.erl:
```
$ c(pong).
```

- Crear el proceso response de pong:
```
$ Pid = spawn(pong, response, []).
```

- Registrar el proceso como algún nombre determinado para accederlo desde ping:
```
$ register(pong_ref, Pid).
```


### 2º Ping

- En una terminal (cmd, linea de comandos) levantar un nodo:
```
$ erl -name ping@ip_computadora -setcookie secret 
```

- Compilar y cargar el archivo ping.erl:
```
$ c(ping).
```

- Ejecutar la funcion init() del módulo ping con el nombre del proceso registrado y el nombre del otro nodo como parámetros
```
$ ping:init(pong_ref, 'pong@ip_computadora').
```
