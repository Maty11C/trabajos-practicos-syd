-module(consumer).
-export([start/1, start/2, stop/0]).

start(Producer) -> % Recibe el nombre registrado del productor (producer)
    start_aux(Producer).

start(Producer, Host) ->
    start_aux({Producer, Host}).

start_aux(Producer) ->
    Consumer = spawn(fun() -> init(Producer) end),
    register(consumer, Consumer).

stop() ->
    consumer ! stop.

init(Producer) ->
    Monitor = monitor(process, Producer),
    Producer ! {hello, self()},
    consumer(0, Monitor).

consumer(ExpectedValue, Monitor) ->
    Next = ExpectedValue+1,
    receive
        {'DOWN', Monitor, process, Object, Info} ->
            io:format("~w died; EXPLOTO!; ~w~n", [Object, Info]),
            consumer(Next, Monitor);
        bye ->
            io:format("killed by producer ~n");
        stop ->
            io:format("suicide ~n");
        {ping, N} ->
            case N > ExpectedValue of
                true ->
                    io:format("warning! expected: ~w - receive: ~w ~n", [ExpectedValue, N]),
                    consumer(Next, Monitor);
                false ->
                    io:format("Number: ~w~n", [ExpectedValue]),
                    consumer(Next, Monitor)
            end
    end.