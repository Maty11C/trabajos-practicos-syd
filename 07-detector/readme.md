# Detector

Un simple detector de fallas en Erlang.

[Enunciado](https://gitlab.com/sistemas_distribuidos/curso/-/blob/master/labs/07-detector.md)

## Inicializando

Es necesario cargar los siguientes módulos:

```
eshell> c(producer).
{ok,producer}
eshell> c(consumer).
{ok,consumer}
```

A continuación, ejecutar la función start(Delay) para iniciar el producer, y start(producer) para el consumer:

```
eshell> producer:start(1000).
true
eshell> consumer:start(producer).
true
```

*Nota: en esta version se imprimira 'Number: <numero>' cada un segundo, comenzando desde 0 (cero)*

## Detectando un crash

Se agrega codigo de Monitor, este se emplea para que al fallar el producer nuestro consumer no quede a la espera infinita de algo. Para ocasionar el crash se agrega simplemente un *case...of* en el producer, donde al recibir el numero 20 se envia a si mismo un mensaje para "crashear".

```
producer(Consumer, N, Delay) ->
    case N =:= 20 of 
        true -> self() ! crash;
        false -> ""
    end,
    receive ->
        crash -> 42/0
    ...
    end.
```

## Un experimento en dos nodos

### En el mismo host

Inicializamos en una terminal (consola, linea de comandos) un nodo llamado silver, en el iniciamos el producer:

```
$ erl -sname silver -setcookie foo

Eshell V11.0  (abort with ^G)
(silver@nombre_pc)1> producer:start(1000).
true
```

Y en otra terminal un nodo llamado gold, donde iniciamos el consumer, en este caso agregando el host del producer:

```
$ erl -sname gold -setcookie foo

Eshell V11.0  (abort with ^G)
(gold@nombre_pc)1> consumer:start(producer, 'silver@nombre_pc').
true
```

Cuando el consumer llega al numero 20 es producer "crashea".

-  ¿Qué mensaje se da como razón cuando el nodo es terminado? ¿Porqué?

En el nodo del consumer el mensaje es justamente el que pusimos en el patern matching, ejemplo:

```
Eshell V11.0  (abort with ^G)  
....
(gold@nombre_pc)3> {producer,'silver@nombre_pc'} died; EXPLOTO!; {badarith,[{producer,producer,3,[{file,[112,114,111,100,117,99,101,114,46,101,114,108]},{line,31}]}]}  
```

Y en el nodo producer se muestra el siguiente error:

```
Eshell V11.0  (abort with ^G)  
....
(silver@nombre_pc)3> =ERROR REPORT==== 26-Apr-2021::20:56:09.493000 ===                           
Error in process <0.91.0> on node 'silver@WAREP250192-AVJ' with exit value:
{badarith,[{producer,producer,3,[{file,"producer.erl"},{line,31}]}]}  
```

Este error hace referencia a la linea en el producer donde se ocaciona el "crasheo":

```
   receive ->
        crash -> 42/0
    ...
    end.
```

El error en el proceso consumer se dispara después de identificar a través del monitor que el proceso producer falló. A partir de la vinculación del monitor con el proceso producer, el proceso consumer es capaz de atrapar el mensaje DOWN cuando la ejecución del proceso producer se detiene por alguna excepción no controlada.

### Un experimento distribuido

Inicializamos el producer en una computadora como nodo silver:

```
$ erl -name silver@ip_computadora_silver -setcookie foo

Eshell V11.0  (abort with ^G)
(silver@ip_computadora_silver)1> producer:start(1000).
true
```

Y en otra computadora en un nodo llamado gold, iniciamos el consumer, agregando el host del producer:

```
$ erl -name gold@ip_computadora_gold -setcookie foo

Eshell V11.0  (abort with ^G)
(gold@ip_computadora_gold)1> consumer:start(producer, 'silver@ip_computadora_silver').
true
```

- ¿Qué sucede si matamos el nodo Erlang en el producer?

Si se produce un crash en el producer notamos las mismas leyendas en los errores que se mostraron estando los nodos en la misma maquina. En cambio si solo matamos el nodo, siendo alguna accion como cerrar la terminal, salir de erlang de alguna manera, el mensaje cambia:

```
Eshell V11.0  (abort with ^G)  
....
(gold@ip_computadora_gold)3> {producer,'silver@192.168.0.143'} died; EXPLOTO!; noconnection
```

- Desconectar la maquina del producer de la red, ¿Qué pasa? Desconectemos por períodos mas largos. ¿Qué pasa ahora?

Si la desconexion es breve, el consumer recibe todo lo que se habia producido durante el periodo de desconexion, y sigue consumiendo normalmente.
Si es mas larga la desconexion, al volver en la terminal del producer aparece el siguiente error:

```
Eshell V11.0  (abort with ^G)  
....
(silver@ip_computadora_silver)2> =ERROR REPORT==== 28-Apr-2021::21:01:16.198186 ===
** Node 'gold@ip_maquina_gold' not responding **
** Removing (timeout) connection ** 
```

Pero de todas formas el consumer tambien vuelve a trabajar, pero ahora el valor esperado es menor al recibido, entonces empieza a mostrar por pantalla el **warning** que en un principio escribimos en el codigo.  
Creemos que se pierden los paquetes al perderse la conexion y al retomarla los valores varian entonces por eso se comenzaria a mostrar este mensaje de warning.

- ¿Qué significa haber recibido un mensaje ’DOWN’? ¿Cuándo debemos confiar en el?

Recibimos un mensaje down seguido de un not connection lo que significa que se perdió la visibilidad entre los nodos.
Sin embargo no es información que el consumer debería tomar como un "DOWN" precisamente, 
puede que se haya caído el proceso o que solamente se haya cortado la conexión y que el consumer no se entere que el producer sigue produciendo.
Consideramos que no hay una respuesta correcta a todos los casos para que el consumer sepa siempre que decisión tomar, pero sí el consumer debería informar lo sucedido y delegar la responsabilidad en el usuario.

- ¿Se recibieron mensajes fuera de orden, aun sin haber recibido un mensaje ’DOWN’? ¿Qué dice el manual acerca de las garantías de envíos de mensajes?

No se recibieron los mensajes fuera de orden, aún así con la conexion perdida por un breve periodo de tiempo, cuando la conexion se restablece el consumer recibe todos los mensajes faltantes de manera ordenada.