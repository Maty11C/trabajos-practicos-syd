# Loggy

[Enunciado](https://gitlab.com/sistemas_distribuidos/curso/-/blob/master/labs/09-loggy.md)

## Inicializando

Compilar los módulos:

```
> rebar3 compile
===> Verifying dependencies...
===> Analyzing applications...
===> Compiling loggy
```

Abrir la consola de rebar3:

```
> rebar3 shell
===> Verifying dependencies...
===> Analyzing applications...
===> Compiling loggy
===> Booted loggy
Eshell V11.2  (abort with ^G)
```

## Test

Para ejecutar los test debemos ejecutar la función run(Sleep, Jitter) del módulo test en la consola de rebar3.

El valor Sleep va a servir para esperar un cierto tiempo (aleatorio a partir del valor proporcionado) a recibir mensajes de otro Worker, una vez finalizado este tiempo el Worker elige un par suyo aleatoriamente, le envía un mensaje y vuelve a disponerse a recibir un mensaje para ejecutarse cíclicamente.
Por otra parte, el valor Jitter corresponderá al tiempo (aleatorio a partir del valor proporcionado) que espera el Worker en enviar un mensaje a un par una vez seleccionado aleatoriamente.

```
1> test:run(100, 100).
```

Corramos algunos tests y tratemos de encontrar mensajes de log que sean mostrados fuera de orden.
- ¿Cómo sabemos que fueron impresos fuera de orden?

Encontramos que mensajes fueron impresos fuera de orden porque se observa una inconsistencia en el orden en que se imprime un mensaje enviado y un mensaje recibido.

En el siguiente resultado, por ejemplo, se imprimen los mensajes de manera invertida. Si se correspondiera con un tiempo lógico debería de imprimirse primero el mensaje sending y luego el mensaje received.

```
...
log: na ringo {received,{hello,57}}
log: na john {sending,{hello,57}} 
...
```

## Tiempo Lamport

Hagamos algunos tests e identifiquemos situaciones donde las entradas de log sean impresas en orden incorrecto.
- ¿Cómo identificamos mensajes que estén en orden incorrecto?
- ¿Qué es siempre verdadero y qué es a veces verdadero?
- ¿Cómo lo hacemos seguro?

Encontramos que mensajes fueron impresos fuera de orden de la misma manera que antes de implementar los tiempos de Lamport y además porque el timer (contador) se imprime de forma desordenada, siendo la correcta de manera ascendente, ejemplo: *1,1,2,3,3,4,5 ...*

En el siguiente resultado, por ejemplo, se imprimen los tiempos de los mensajes de manera invertida.

```
...
log: 2 ringo {received,{hello,57}}
log: 1 john {sending,{hello,57}} 
...
```

Implementados los tiempos de Lamport vemos que lo que es siempre verdadero es que, en un mismo worker el orden de los mensajes se corresponde con el orden de su contador. Por ejemplo en la siguiente ejecución vemos que el orden del contador del worker john corresponde con el orden de los mensajes, es decir, que primero john envió un mensaje y luego recibió otro, tal como lo indica su contador primero en 1 y luego en 4.
```
...
log: 2 ringo {received,{hello,57}}
log: 1 john {sending,{hello,57}} 
log: 4 john {received,{hello,77}}
log: 1 paul {sending,{hello,68}}
...
```

Por el contrario, lo que es a veces verdadero es que para distintos workers, el orden de los contadores se correspondan con el orden de los mensajes que se fueron enviando y recibiendo concurrentemente. Por ejemplo en la siguiente ejecución vemos que el orden de los contadores de los workers paul y jhon en un momento no se corresponden con el orden de los mensajes (paul recibe el mensaje cuando su contador se encuentra en 6 y posteriormente jhon se lo envía con su contador en 5).

```
...
log: 6 paul {received,{hello,90}}
log: 3 ringo {sending,{hello,77}} 
log: 4 ringo {received,{hello,68}}
log: 5 ringo {received,{hello,58}}
log: 7 paul {sending,{hello,40}}
log: 1 george {sending,{hello,58}}
log: 5 john {sending,{hello,90}}  
...
```

Mientras que el orden de sus contadores en otro momento sí se corresponden (paul envía un mensaje cuando su contador se encuentra en 7 y posteriormente jhon lo recibe cuando su contador está en 8).

```
...
log: 7 paul {sending,{hello,40}}
log: 1 george {sending,{hello,58}}
log: 5 john {sending,{hello,90}}  
log: 8 john {received,{hello,40}} 
...
```

Para lograr el logueo correcto de los mensajes implementamos una forma de ordenar los mensajes a medida que se van recibiendo en el logger_


## La parte delicada

- ¿Cómo sabemos si los mensajes son seguros de imprimir?

Una forma de saberlo es llevando control sobre cada uno de los nodos que se comunican y cual fue su ultimo contador, entonces de esta manera poder imprimir los mensajes que son menores a esos contadores, asi es como se plantea en el enunciado. Para lograrlo nosotros decidimos utilizar un estructura de datos de pares en una lista. Se agregaron las funciones dadas en el modulo *time.erl*, y a su vez para el manejo de nuestra estructura de datos se implemento un nuevo modulo llamado *clock.erl*, a este ultimo solo lo conoce *time.erl*, por ende seria facil cambiar la estructura sin que el resto de los modulos se dieran cuenta.

## En el curso

- Escribir un reporte que describa el módulo *time*. Describir si encontraron entradas fuera de orden en la primera implementación y en caso afirmativo, cómo fueron detectadas.

---
### Módulo: time.erl

Se utiliza para manejo de los contadores Lamport de cada nodo por separado y tambien para control de todos a la vez. En este módulo encontramos las siguientes funciones:

- **zero():** retorna un valor Lamport inicial (puede ser 0).
- **inc(Name, T):** retorna el tiempo T incrementado en uno (probablemente ignoremos el Name, pero lo usaremos más adelante).
- **merge(Ti, Tj):** unifica los dos timestamps Lamport (eso es, toma el mayor).
- **leq(Ti, Tj):** retorna true si Ti es menor o igual a Tj.

Estas anteriores funciones son de la primer implementación, ya dada. Al no tener control concreto sobre el tiempo de todos los nodos a la vez, se encontraron mensajes desordenados entre nodos, y como se comento antes un orden correcto entre mensajes del mismo nodo.

Al módulo se le agregaron las funciones pedidas:

- **clock(Nodes):** retorna un reloj que pueda llevar cuenta de los nodos
- **update(Node, Time, Clock):** retorna un reloj que haya sido actualizado dado que hemos recibido un mensaje de log de un nodo en determinado momento.
- **safe(Time, Clock):** retorna true o false si es seguro enviar el mensaje de log de un evento que ocurrió en el tiempo Time dado.

Para el manejo de todos los tiempos de los nodos a la vez fue necesario implementar una estructura de datos aparte, lo cual se logro agregando el módulo *clock.erl* con las siguientes funciones:

> La estructura es una lista de pares:  
[{Node_1, Time},{Node_2, Time},...,{Node_n, Time}]

- **start(Nodes):** recibe la lista de nodos y los inicializa, retorna la lista de pares.
- **update(Clock, Node, Time):** recibe una lista de pares, entonces actualiza el nodo con el tiempo, retorna la lista de pares actualizada.
- **safe(Clock, Time):** recibe la lista de pares y un tiempo, si es que todos los nodos superaron el valor del tiempo recibido retorna *true*, si no *false*.

--- 

- ¿Qué es lo que el log final nos muestra?

El log final nos muestra los mensajes ordenados tanto para un mismo nodo como para con el resto:

```
1> test:run(1000,1000).
log: 1 john {sending,{hello,57}} 
log: 1 paul {sending,{hello,68}}
log: 1 george {sending,{hello,58}}
log: 2 george {sending,{hello,100}}
log: 2 ringo {received,{hello,57}}
log: 3 ringo {sending,{hello,77}}
log: 4 john {received,{hello,77}}
log: 4 ringo {received,{hello,68}}
log: 5 john {sending,{hello,90}}
log: 5 ringo {received,{hello,58}}
log: 6 paul {received,{hello,90}}
log: 6 ringo {sending,{hello,42}}
log: 7 paul {sending,{hello,40}}
log: 7 ringo {received,{hello,100}}
log: 8 john {received,{hello,40}}
...
```
- ¿Los eventos ocurrieron en el mismo orden en que son presentados en el log?

No, los eventos pudieron haber pasado en otro orden e imprimirse de forma incorrecta sin el ordenamiento y control sobre todos los nodos a la vez que implementamos.

- ¿Que tan larga será la cola de retención? Hacer algunos tests para tratar de encontrar el máximo número de entradas.

Haciendo unos test podemos apreciar que el numero max en la cola aumenta cada vez mas dependiendo la cantidad de nodos. Ej:  

| Cantidad nodos    | Largo maximo de la cola    |
|-------------------|----------------------------|
| 2                 | 3-4                        |
| 4                 | 16-35                      |
| 10                | 100-130                    |

## Vectores de relojes

- ¿Qué diferencias habrían si se hubieran utilizado vectores de relojes?

No habrían grandes diferencias si implementaramos relojes vectoriales en vez de relojes lógicos de Lamport ya que la estructura planteada desde un principio desacopló lo que eran los procesos y el logger, de la lógica del reloj. De esta manera bastó con reemplazar la implementación anterior de timer/contador por vectores en el módulo 'time' y no hizo falta adaptar a los modulos 'logger' y 'worker' para que loggy siga funcionando correctamente.

Para implementar el reloj vectorial se optó por utilizar como estructura una lista de tuplas {Nodo, Vector} como la siguiente:

```
[ 
    { NameNode_1, [ {NameNode_1, Vector_1}, ... {NameNode_n, Vector_n} ] },
    ...
    { NameNode_n, [ {NameNode_1, Vector_1}, ... {NameNode_n, Vector_n} ] },
]
```

*Los implementación de relojes vectoriales se encuentra en la rama [loggy-vector](https://gitlab.com/Maty11C/trabajos-practicos-syd/-/tree/loggy-vector)*