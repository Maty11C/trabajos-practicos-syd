-module(clock).
-export([start/1, update/3, safe/2]).

start(Nodes) ->
    lists:map(fun(Node) -> {Node, 1} end, Nodes).

update(Clock, Node, Time) ->
    lists:keyreplace(Node, 1, Clock, {Node, Time}). % Se actualiza el nodo con el nuevo tiempo

safe(Clock, Time) ->
    lists:all(fun({_, TimeNode}) -> TimeNode >= Time end, Clock). % Es seguro si los contadores de todos los workers superaron el valor del contador del mensaje recibido