-module(worker).
-export([start/5, stop/1, peers/2]).

start(Name, Logger, Seed, Sleep, Jitter) ->
    spawn_link(fun() -> init(Name, Logger, Seed, Sleep, Jitter) end).

stop(Worker) ->
    Worker ! stop.

init(Name, Log, Seed, Sleep, Jitter) ->
    random:seed(Seed, Seed, Seed),
    receive
        {peers, Peers} ->
            loop(Name, Log, Peers, Sleep, Jitter, time:zero()); % Se inicializa el timer/contador
        stop ->
            ok
    end.

peers(Wrk, Peers) ->
    Wrk ! {peers, Peers}.

loop(Name, Log, Peers, Sleep, Jitter, Timer)->
    Wait = random:uniform(Sleep),
    receive
        {msg, ReceivedTimer, Msg} ->
            NewTimer = time:inc(Name, time:merge(Timer, ReceivedTimer)), % Se incrementa el timer/contador una vez actualizado en base al timer/contador del worker emisor del mensaje
            Log ! {log, Name, NewTimer, {received, Msg}},
            loop(Name, Log, Peers, Sleep, Jitter, NewTimer);
        stop ->
            ok;
        Error ->
            Log ! {log, Name, time, {error, Error}}
    after Wait ->
            Selected = select(Peers),
            Message = {hello, random:uniform(100)},
            NewTimer = time:inc(Name, Timer), % Se incrementa el timer/contador
            Selected ! {msg, NewTimer, Message}, % Se envía el valor actual del timer/contador en el mensaje al worker receptor
            jitter(Jitter),
            Log ! {log, Name, NewTimer, {sending, Message}},
            loop(Name, Log, Peers, Sleep, Jitter, NewTimer)
    end.

select(Peers) ->
    lists:nth(random:uniform(length(Peers)), Peers).

jitter(0) -> ok;
jitter(Jitter) -> timer:sleep(random:uniform(Jitter)).

