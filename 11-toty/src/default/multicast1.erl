-module(multicast1).

-export([start/0]).

start() -> spawn(fun () -> init() end).

init() ->
    receive
        {peers, Master, Nodos, Jitter} -> loop(Master, Nodos, Jitter);
        stop -> ok
    end.

loop(Master, Nodos, Jitter) ->
    receive
        {send, Msg} ->
            requests(Nodos, Msg, Jitter),
            loop(Master, Nodos, Jitter);
        {msg, Msg} ->
            case whereis(Master) of
                undefined ->
                    io:format("undefined: the process ~s is dead ~n", [Master]);
                Pid ->
                    Pid ! { received, Msg} 
            end,
            loop(Master, Nodos, Jitter);
        stop ->
            ok_multicast
    end.

requests(Nodos, Msg, Jitter) ->
    lists:map(fun(Node) -> Node ! {msg, Msg}, timer:sleep(rand:uniform(Jitter)) end, Nodos).