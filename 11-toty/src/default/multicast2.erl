-module(multicast2).

-export([start/0]).

start() -> spawn(fun () -> init() end).

init() ->
    receive
        {peers, Master, Nodes, Jitter} -> 
            loop(Master, {0, self()}, Nodes, [], [], Jitter);
        stop -> 
            ok
    end.

loop(Master, Next, Nodes, Cast, Queue, Jitter) ->
    receive
        {send, Msg} ->
            Ref = make_ref(),
            requests(Nodes, Ref, Msg, Jitter),
            Cast2 = cast(Cast, length(Nodes), Ref),
            loop(Master, Next, Nodes, Cast2, Queue, Jitter);
        {request, From, Ref, Msg} ->
            From ! { proposal, Ref, Next },
            Queue2 = insert(Queue, Next, Ref, {From, Msg}),
            Next2 = increment(Next),
            loop(Master, Next2, Nodes, Cast, Queue2, Jitter);
        {proposal, Ref, Proposal} ->
            case proposal(Cast, Ref, Proposal) of
                {agreed, Seq, Cast2} ->
                    agree(Nodes, Ref, Seq),
                    loop(Master, Next, Nodes, Cast2, Queue, Jitter);
                Cast2 ->
                    loop(Master, Next, Nodes, Cast2, Queue, Jitter)
            end;
        {agreed, Ref, Seq} ->
            Updated = update(Queue, Ref, Seq),
            {Agreed, Queue2} = agreed(Updated, Seq),
            deliver(Master, Agreed),
            Next2 = increment(Next, Seq),
            loop(Master, Next2, Nodes, Cast, Queue2, Jitter);
        stop ->
            ok_multicast
    end.

requests(Nodes, Ref, Msg, Jitter) ->
    lists:map(fun(Node) -> Node ! { request, self(), Ref, Msg }, timer:sleep(rand:uniform(Jitter)) end, Nodes).

cast(Cast, L, Ref) -> 
    [{ Ref, L, 0 } | Cast]. % [{ Ref, Nodos que restan contestar, Numero de secuencia inicial } | Cast]

insert(Queue, {Seq, _ }, Ref, FromMsg) -> % FromMsg === {From, Msg}
    sortedForSeq([{Ref, false, Seq, FromMsg} | Queue]).

increment({N, Id}) ->
    M = N +1,
    {M, Id}.

increment({N, Id}, SeqAgreed) ->
    M = max(N, SeqAgreed) +1,
    {M, Id}.
    
% 1. Se actualiza el conjunto Cast.
% 2. Si se alcanzo un acuerdo se devuelve {agreed, Seq, Cast2}, sino solo se devuelve Cast2
proposal(Cast, Ref, {Seq, _}) -> % Proposal == {Seq, From}
    case lists:keysearch(Ref, 1, Cast) of
        {value, { _, L, Sofar}} ->
            NewL = L - 1, % Se decrementa el contador de los nodos que tenian que contestar
            NewSofar = max(Seq, Sofar), % Me quedo con el máximo visto hasta el momento
            Cast2 = lists:keyreplace(Ref, 1, Cast, {Ref, NewL, NewSofar}), 
            if
                NewL == 0 -> % Ya todos enviaron su propuesta
                   {agreed, NewSofar, Cast2};
                true ->
                   Cast2
            end;
        false ->
            Cast
    end.

agree(Nodes, Ref, Seq) ->
    lists:map(fun(Node) -> Node ! {agreed, Ref, Seq} end, Nodes).

update(Queue, Ref, Seq) ->
    %io:format("update: ~w | QUEUE: ~w | REF AGREED: ~w | SEQ AGREED: ~w ~n", [self(), Queue, Ref, Seq]),
    case lists:keysearch(Ref, 1, Queue) of
        {value, {Ref, _, _, FromMsg}} ->
            Queue2 =lists:keyreplace(Ref, 1, Queue, {Ref, true, Seq, FromMsg}),
            %io:format("update: ~w | QUEUE UPDATE: ~w ~n", [self(), Queue2]),
            Sorted = sortedForSeq(Queue2),
            %io:format("update: ~w | QUEUE SORTED: ~w ~n", [self(), Sorted]),
            Sorted;
        false ->
            Queue
    end.

agreed(Updated, SeqAgreed) ->
    {Agreed, Queue2} = lists:partition(fun({_, Value, Seq, _}) -> (Value == true) and (SeqAgreed == Seq) end, Updated),
    %io:format("agreed: ~w | QUEUE AGREED: ~w | QUEUE REST: ~w ~n", [self(), Agreed, Queue2]),
    {Agreed, Queue2}.

deliver(Master, Agreed) ->
    lists:map(
        fun({_, _, _, {From, Msg}}) -> 
            case whereis(Master) of
                undefined ->
                    io:format("undefined: the process ~s is dead ~n", [Master]);
                Pid ->
                    Pid ! { received, Msg, From } 
            end
        end, Agreed).

sortedForSeq(List) ->
    lists:sort(fun({_, _, Seq1, _}, {_, _, Seq2, _}) -> Seq1 =< Seq2  end, List).