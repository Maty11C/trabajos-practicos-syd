%%%-------------------------------------------------------------------
%% @doc toty public API
%% @end
%%%-------------------------------------------------------------------

-module(toty_app).

-behaviour(application).

-export([start/2, stop/1]).

start(_StartType, _StartArgs) ->
    toty_sup:start_link().

stop(_State) ->
    ok.

%% internal functions
