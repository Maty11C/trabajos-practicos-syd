-module(lock3).
-export([start/1]).

start(Id) -> spawn(fun () -> init(Id) end).

init(Id) ->
    Timer = time:zero(),
    receive
        {peers, Peers} -> 
            open(Id, Peers, Timer);
        stop -> ok
    end.

open(Id, Nodes, Timer) ->
    receive
        {take, Master} ->
            Refs = requests(Id, Nodes, Timer),
            NewTimer = time:inc(Timer, length(Nodes)), % Se incrementa el timer/contador
            wait(Id, Nodes, Master, Refs, [], NewTimer);
        {request, From, Ref, _, ReceivedTimer} ->
            NewTimer = time:inc(time:merge(Timer, ReceivedTimer), 1),
            From ! {ok, self(), Ref},
            open(Id, Nodes, NewTimer);
        stop ->
            ok
    end.

requests(Id, Nodes, Timer) ->
    lists:map(fun(P) ->
            R = make_ref(),
            P ! {request, self(), R, Id, Timer},
            {P, R} % {Node, Ref}
        end, Nodes).

wait(Id, Nodes, Master, [], Waiting, Timer) ->
        Master ! taken,
        held(Id, Nodes, Waiting, Timer);

wait(Id, Nodes, Master, Refs, Waiting, Timer) ->
    receive
        {request, From, Ref, FromId, ReceivedTimer} ->
            Leq = time:leq(ReceivedTimer, Timer),
            if
                Leq or ((ReceivedTimer == Timer) and (FromId < Id)) -> % Si el lock recibe una request de un lock con un timer menor
                    From ! {ok, self(), Ref}, % Le envía un ok al lock con el timer menor
                    NewRef = make_ref(), % Se crea una nueva referencia para reenviar una request
                    NewTimer = time:inc(Timer, 1), % Se incrementa el timer
                    From ! {request, self(), NewRef, Id, NewTimer}, % Se le envía una request al lock de timer menor para que lo tenga en cuenta
                    Refs2 = lists:keydelete(From, 1, Refs), % Se borra la referencia vieja
                    wait(Id, Nodes, Master, [{From, NewRef}|Refs2], Waiting, NewTimer); % Se agrega la referencia nueva
                true ->
                    NewTimer = time:inc(time:merge(Timer, ReceivedTimer), 1),
                    wait(Id, Nodes, Master, Refs, [{From, Ref}|Waiting], NewTimer)
            end;
        {ok, From, Ref} ->
            Refs2 = lists:delete({From, Ref}, Refs),
            wait(Id, Nodes, Master, Refs2, Waiting, Timer);
        release ->
            ok(Waiting),
            open(Id, Nodes, Timer)
   end.

ok(Waiting) ->
    lists:foreach(fun({F,R}) -> F ! {ok, self(), R} end, Waiting).

held(Id, Nodes, Waiting, Timer) ->
    receive
        {request, From, Ref, _, ReceivedTimer} ->
            NewTimer = time:inc(time:merge(Timer, ReceivedTimer), 1),
            held(Id, Nodes, [{From, Ref}|Waiting], NewTimer);
        release ->
            ok(Waiting),
            open(Id, Nodes, Timer)
    end.
