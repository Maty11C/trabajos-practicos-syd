-module(client).
-export([start/3, read/2, write/3, commit/1, test/3]).

start(NameClient, Server, Test) ->
    Client = spawn(fun() -> init(Server, Test) end),
    register(NameClient, Client).

init(Server, Test) ->
    Handler = server:open(Server),
    Ref = make_ref(),
    if
        Test =/= {} ->
            client(Handler, Ref, Test);
        true ->
            client(Handler, Ref)
    end.
    
client(Handler, Ref) ->
    receive
        {read, EntryNumber, Test} -> 
            Handler ! {read, Ref, EntryNumber},
            client(Handler, Ref, Test);
        {write, EntryNumber, Value, Test} -> 
            Handler ! {write, EntryNumber, Value},
            client(Handler, Ref, Test);
        {commit, Test} ->
            Handler ! {commit, Ref},
            client(Handler, Ref, Test);

        {read_response, Reference , Entry, Value, Time} -> 
            io:format("READ ~n ref: ~w ~n reference: ~w ~n entry: ~w ~n value: ~w ~n time: ~w ~n", [Ref, Reference, Entry, Value, Time]),
            client(Handler, Ref);
        {Reference, ok} ->
            io:format("Commit ok para: ~w ~n", [Reference]);
        {Reference, abort} ->
            io:format("Commit abort para: ~w ~n", [Reference])
    end.

read(NameClient, N) ->
    NameClient ! {read, N, {}},
    ok.

write(NameClient, N, Value) ->
    NameClient ! {write, N, Value, {}},
    ok.

commit(NameClient) ->
    NameClient ! {commit, {}},
    ok.

%% TEST CLIENT

client(Handler, Ref, Test) ->
    receive
        {read, EntryNumber, Test} -> 
            Handler ! {read, Ref, EntryNumber},
            client(Handler, Ref, Test);
        {write, EntryNumber, Value, Test} -> 
            Handler ! {write, EntryNumber, Value},
            client(Handler, Ref, Test);
        {commit, Test} ->
            Handler ! {commit, Ref},
            client(Handler, Ref, Test);

        {read_response, _, _, _, _} -> 
            if Test =/= {} ->
                Test ! {termine, "read"}
            end,
            client(Handler, Ref, Test);
        {write_response} ->
            if Test =/= {} ->
                Test ! {termine, "write"}
            end,
            client(Handler, Ref, Test);
        {_, ok} ->
            if Test =/= {} ->
                Test ! {termine, "commit-ok"}
            end;
        {_, abort} ->
            if Test =/= {} ->   
                Test ! {termine, "commit-abort"}
            end
    end.

test(Transaction, NameClient, Test) ->
    case Transaction of
        read -> NameClient ! {read, 1, Test}; % Lee el valor del índice 1 del store
        write -> NameClient ! {write, 1, 5, Test}; % Escribe el valor "5" en el índice 1 del store
        commit -> NameClient ! {commit, Test}
    end.