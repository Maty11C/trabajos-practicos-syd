-module(handler).
-export([start/3]).

start(Client, Validator, Store) ->
    spawn_link(fun() -> init(Client, Validator, Store) end).

init(Client, Validator, Store) ->
    handler(Client, Validator, Store, [], []).

handler(Client, Validator, Store, Reads, Writes) ->
    receive
        {read, Ref, N} ->
            case lists:keysearch(N, 1, Writes) of
                {value, {N, _, Value}} -> % La entrada N ya fue escrita
                    Entry = store:lookup(N, Store),
                    self() ! {Ref, Entry, Value, make_ref(), true},
                    handler(Client, Validator, Store, Reads, Writes);
                false -> % La entrada N todavía no fue escrita
                    Entry = store:lookup(N, Store),
                    Entry ! {read, Ref, self()},
                    handler(Client, Validator, Store, Reads, Writes)
            end;
        {Ref, Entry, Value, Time, Quick} ->
            Client ! {read_response, Ref, Entry, Value, Time},
            if
                Quick ->
                    handler(Client, Validator, Store, Reads, Writes);
                true ->
                    handler(Client, Validator, Store, [{Entry, Time}|Reads], Writes) % Se guarda la entrada y el tiempo en el conjunto de lecturas
            end;
        {write, N, Value} ->
            Added = [{N, store:lookup(N, Store), Value}|Writes],
            handler(Client, Validator, Store, Reads, Added); % Se guarda la entrada con índice N y el valor en el conjunto de escrituras
        {commit, Ref} ->
            Validator ! {validate, Ref, Reads, Writes, Client}
        % {Ref, ok} ->
        %     Client ! {commit_ok, Ref};
        % {Ref, abort} ->
        %     Client ! {commit_abort, Ref}
    end.
