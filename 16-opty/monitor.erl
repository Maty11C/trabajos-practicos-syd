-module(monitor).
-export([start/1]).

start(Test) ->
    spawn(fun() -> monitor(0,0,0,0, Test) end).

monitor(Transactions, Reads, Success, Aborts, Test) ->
    Test ! {monitor, self()},
    receive
        { read, {_, _, _, _}} ->
            monitor(Transactions, Reads +1, Success, Aborts, Test);
        { commit_ok } ->
            Test ! termine,
            monitor(Transactions +1, Reads, Success +1, Aborts, Test);
        { commit_abort } ->
            Test ! termine,
            monitor(Transactions +1, Reads, Success, Aborts +1, Test);
        result ->
            io:format(" TRANSACTIONS: ~w ~n READS: ~w ~n COMMITS OK: ~w ~n COMMITS ABORT: ~w ~n HIT %: ~w ~n ABORT %: ~w ~n", 
                [Transactions, Reads, Success, Aborts, ((Success/Transactions)*100),((Aborts/Transactions)*100)])
    end.