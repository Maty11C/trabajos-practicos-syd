-module(server).
-export([start/1, open/1, stop/0]).

start(N) ->
    Server = spawn(fun() -> init(N) end),
    register(server, Server).
    
init(N) ->
    Store = store:new(N),
    Validator = validator:start(),
    server(Validator, Store).

open(Server) ->
    Server ! {open, self()},
    receive
        {transaction, Validator, Store} ->
            handler:start(self(), Validator, Store)
    end.

server(Validator, Store) ->
    receive
        {open, Client} ->
            Client ! {transaction, Validator, Store},
            server(Validator, Store);
        stop ->
            store:stop(Store)
    end.

stop() ->
    self() ! stop.