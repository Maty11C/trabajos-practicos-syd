-module(multicast).

-export([start/0]).

start() -> spawn(fun () -> init() end).

init() ->
    receive
        {peers, Master, Nodes} -> 
            loop(Master, Nodes, {}, [], {});
        stop -> 
            ok
    end.

loop(Master, Nodes, Ref, Queue, VehicleMonitor) ->
    receive
        monitor ->
            VehicleMonitor2 = monitor(process, Master),
            loop(Master, Nodes, {}, [], VehicleMonitor2);
        race ->
            Ref2 = make_ref(),
            proposals(Nodes, Ref2),
            loop(Master, Nodes, Ref2, Queue, VehicleMonitor);
        {proposal, From, ProposedRef} ->
            Queue2 = sorted([{From, ProposedRef} | Queue]),
            case result(Ref, Queue2, length(Nodes)) of
                winner ->
                    Master ! winner,
                    loop(Master, Nodes, {}, [], VehicleMonitor);
                loser ->
                    Master ! loser,
                    loop(Master, Nodes, {}, [], VehicleMonitor);
                unfinished ->
                    ok,
                    loop(Master, Nodes, Ref, Queue2, VehicleMonitor)
            end;
        {update, Nodes2} ->
            loop(Master, Nodes2, Ref, Queue, VehicleMonitor);
        {'DOWN', VehicleMonitor, process, _, _} ->
            down(Nodes)
    end.

down(Nodes) ->
    Nodes2 = lists:delete(self(), Nodes),
    lists:foreach(fun(Node) -> Node ! { update, Nodes2 } end, Nodes2).

proposals(Nodes, Ref) ->
    lists:map(fun(Node) -> Node ! { proposal, self(), Ref } end, Nodes).

sorted(Queue) ->
    lists:sort(fun({_, Proposal1}, {_, Proposal2}) -> Proposal1 < Proposal2  end, Queue).

result(Ref, Queue, LengthNodes) ->
    [{_, WinningRef} | _ ] = Queue,
    % io:format("[~w] Queue: ~w. Ref: ~w. WinRef: ~w ~n", [Master, Queue, Ref, WinningRef]),
    % if
    %     (WinningRef /= Ref) -> 
    %         loser;
    %     true ->
    %         if 
    %             (length(Queue) == LengthNodes) and (WinningRef == Ref) ->
    %                 winner;
    %             true ->
    %                 unfinished
    %         end
    % end.
    if
        (Ref == {}) or (length(Queue) < LengthNodes) ->
            unfinished;
        true ->
            if 
                WinningRef == Ref ->
                    winner;
                true ->
                    loser
            end
    end.