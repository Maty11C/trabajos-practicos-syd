-module(vehicle).
-export([start/4, stop/1, request/4]).

start(Name, Multicast, Map, Location) ->
    Vehicule = spawn(fun() -> init(Name, Multicast, Map, Location) end),
    register(Name, Vehicule).

stop(Vehicle) ->
    Vehicle ! stop.

request(Name, Passenger, PassengerLocation, PassengerDestination) ->
    Name ! { request, Passenger, PassengerLocation, PassengerDestination }.

init(Name, Multicast, Map, {Location, Coordinates}) ->
    io:format("~w inicializado en ~s ~n", [Name, Location]),
    Multicast ! monitor,
    map:spawn(Map, Name, {Location, Coordinates}),
    wait(Name, Multicast, Map).

wait(Name, Multicast, Map)->
    receive
        { request, NamePassenger, {Location, CoordinatesLocation}, {Destination, CoordinatesDestination} } ->
            io:format("~w recibe pedido de cliente ~w. Desea ir de ~s a ~s. ~n", [Name, NamePassenger, Location, Destination]),
            race(Name, Multicast, NamePassenger, Map, {Location, CoordinatesLocation}, {Destination, CoordinatesDestination});
        stop ->
            io:format("~w se deconecta ~n", [Name]),
            Multicast ! stop,
            ok
    end.

race(Name, Multicast, NamePassenger, Map, PassengerLocation, PassengerDestination) ->
    io:format("~w entra en competencia para atender pedido de usuario ~w ~n", [Name, NamePassenger]),
    timer:sleep(rand:uniform(2000)),
    Multicast ! race,
    receive
        winner ->
            io:format("Soy ~w , gane! ~n", [Name]),
            Location = map:location(Map, self(), Name),
            NamePassenger ! { accept, Name, Location },
            busy(Name, Multicast, NamePassenger, Map, Location, PassengerLocation, PassengerDestination);
        loser ->
            io:format("Soy ~w , perdi! ~n", [Name]),
            wait(Name, Multicast, Map)
    end.

busy(Name, Multicast, NamePassenger, Map, Location, PassengerLocation, PassengerDestination) ->
    simulate_tour(Map, Location, PassengerLocation),
    simulate_down(),
    NamePassenger ! arrives,
    receive
        board ->
            io:format("~w sube al vehiculo ~w ~n", [NamePassenger, Name]),
            travel(Name, Multicast, NamePassenger, Map, PassengerLocation, PassengerDestination)
    end.

travel(Name, Multicast, NamePassenger, Map, PassengerLocation, PassengerDestination) ->
    simulate_tour(Map, PassengerLocation, PassengerDestination),
    io:format("~w llega a destino ~n", [Name]),
    NamePassenger ! reached,
    map:update(Map, Name, PassengerDestination),
    wait(Name, Multicast, Map).

simulate_tour(Map, {FromLocation, FromCoordinates}, {ToLocation, ToCoordinates}) ->
    Map ! { time, self(), {FromLocation, FromCoordinates}, {ToLocation, ToCoordinates} },
    receive
        { time, Time } ->
            io:format("[Simulando viaje de ~s a ~s. Tiempo estimado: ~w segundos]... ~n~n", [FromLocation, ToLocation, Time/1000]),
            timer:sleep(Time)
    end.

simulate_down() ->
    Result = lists:nth(rand:uniform(3), [continue, continue, down]),
    if
        (Result == continue) -> 
            continue;
        true ->
            exit(kill)
    end.