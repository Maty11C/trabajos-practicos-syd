-module(test).
-export([start/2]).

start(Vehicles, Destination) ->
    io:format("Run: start test ~n"),
    test_request(Vehicles, Destination).

test_request(Vehicles, Destination) ->
    {NamePassenger, NameVehicles} = ubar:start(Vehicles),
    timer:sleep(2000),
    passenger:request(passenger_01, Destination),
    timer:sleep(60000),
    ubar:stop(NamePassenger, NameVehicles).