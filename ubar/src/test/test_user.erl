-module(test_user).
-export([start/0]).

start() ->
    io:format("Run: start test_user ~n"),
    data:start(data),
    test_user_start(),
    test_user_request(),
    test_user_stop(),
    data:stop(data).


% Inicializar un usuario
test_user_start() ->
    io:format("Run case: user_start  ~n"),
    user:start(passenger_01).

% Un usuario pide un auto y no hay ninguno.
test_user_request() ->
    io:format("Run case: vehicle_request  ~n"),
    user:request(passenger_01).

% Un vehiculo se desconecta
test_user_stop() ->
    io:format("Run case: user_stop  ~n"),
    user:stop(passenger_01).