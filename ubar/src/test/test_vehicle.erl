-module(test_vehicle).
-export([start/0]).

start() ->
    io:format("Run: start test_vehicle ~n"),
    data:start(data),
    test_vehicle_start(),
    test_vehicle_request(),
    test_vehicle_stop(),
    data:stop(data).


% Inicializar un vehiculo para que espere un pedido
test_vehicle_start() ->
    io:format("Run case: start_vehicle  ~n"),
    vehicle:start(vehicle_01).

% Un vehiculo recibe un pedido
test_vehicle_request() ->
    io:format("Run case: vehicle_request  ~n"),
    vehicle:request(vehicle_01, passenger_01).

% Un vehiculo se desconecta
test_vehicle_stop() ->
    io:format("Run case: vehicle_stop  ~n"),
    vehicle:stop(vehicle_01).